import sort_lib
import unittest


class TestSortingAlgorithms(unittest.TestCase):
    def test_empty_list(self):
        input_list = []
        output_list = []
        self.assertEqual(sort_lib.bubble_sort(input_list), output_list)
        self.assertEqual(sort_lib.selection_sort(input_list), output_list)
        self.assertEqual(sort_lib.merge_sort(input_list), output_list)

    def test_list_one_element(self):
        input_list = [1]
        output_list = [1]
        self.assertEqual(sort_lib.bubble_sort(input_list), output_list)
        self.assertEqual(sort_lib.selection_sort(input_list), output_list)
        self.assertEqual(sort_lib.merge_sort(input_list), output_list)

    def test_list_two_elements(self):
        input_list = [2, 3]
        output_list = [3, 2]
        self.assertEqual(sort_lib.bubble_sort(input_list), output_list)
        self.assertEqual(sort_lib.selection_sort(input_list), output_list)
        self.assertEqual(sort_lib.merge_sort(input_list), output_list)

    def test_list_of_same_elements(self):
        input_list = [1, 1, 1]
        output_list = [1, 1, 1]
        self.assertEqual(sort_lib.bubble_sort(input_list), output_list)
        self.assertEqual(sort_lib.selection_sort(input_list), output_list)
        self.assertEqual(sort_lib.merge_sort(input_list), output_list)

    def test_list_of_same_elements_2(self):
        input_list = [1, 2, 1]
        output_list = [2, 1, 1]
        self.assertEqual(sort_lib.bubble_sort(input_list), output_list)
        self.assertEqual(sort_lib.selection_sort(input_list), output_list)
        self.assertEqual(sort_lib.merge_sort(input_list), output_list)

    def test_list_increasing(self):
        input_list = [1, 2, 3, 4, 5]
        output_list = [5, 4, 3, 2, 1]
        self.assertEqual(sort_lib.bubble_sort(input_list), output_list)
        self.assertEqual(sort_lib.selection_sort(input_list), output_list)
        self.assertEqual(sort_lib.merge_sort(input_list), output_list)

    def test_list_decreasing(self):
        input_list = [5, 4, 3, 2, 1]
        output_list = [5, 4, 3, 2, 1]
        self.assertEqual(sort_lib.bubble_sort(input_list), output_list)
        self.assertEqual(sort_lib.selection_sort(input_list), output_list)
        self.assertEqual(sort_lib.merge_sort(input_list), output_list)

    def test_list(self):
        input_list = [21, 12, 33, 55, 22]
        output_list = [55, 33, 22, 21, 12]
        self.assertEqual(sort_lib.bubble_sort(input_list), output_list)
        self.assertEqual(sort_lib.selection_sort(input_list), output_list)
        self.assertEqual(sort_lib.merge_sort(input_list), output_list)

    def test_list_2(self):
        input_list = [42, 9, 17, 54, 602, -3, 54, 999, -11]
        output_list = [999, 602, 54, 54, 42, 17, 9, -3, -11]
        self.assertEqual(sort_lib.bubble_sort(input_list), output_list)
        self.assertEqual(sort_lib.selection_sort(input_list), output_list)
        self.assertEqual(sort_lib.merge_sort(input_list), output_list)

suite = unittest.TestLoader().loadTestsFromTestCase(TestSortingAlgorithms)
unittest.TextTestRunner(verbosity=2).run(suite)
