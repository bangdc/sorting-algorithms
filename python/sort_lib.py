def bubble_sort(the_list):
    """
    :param the_list: list of number
    :return: list of number in descending order
    Bubble sorting algorithm:
        - Compare consecutive pairs of elements
        - Swap them
        - Reach end of list, do over again, until no more swap happens.
        - Complexity: O(n^2)
    """
    descending_list = the_list[:]
    no_more_swap = False
    while not no_more_swap:
        no_more_swap = True
        for i in range(1, len(descending_list)):
            if descending_list[i] > descending_list[i - 1]:
                no_more_swap = False
                temp = descending_list[i]
                descending_list[i] = descending_list[i - 1]
                descending_list[i - 1] = temp
    return descending_list

def selection_sort(the_list):
    """

    :param the_list: list of number
    :return: list of number in descending order
    Selection sorting algorithm:
        - Each loop, find the LARGEST number
        - Swap LARGEST number with the beginning number.
        - At the ith step: (It is also the loop invariant
            - First i elements in the list are sorted
            - All left elements from ith are bigger than the sorted elements
        - Complexity: O(n^2)
    """
    descending_list = the_list[:]
    list_length = len(descending_list)
    for i in range(list_length - 1):
        largest_index = i
        for j in range(i+1, list_length):
            if descending_list[j] > descending_list[largest_index]:
                largest_index = j
        temp = descending_list[i]
        descending_list[i] = descending_list[largest_index]
        descending_list[largest_index] = temp
    return descending_list

def merge_sort(the_list):
    """

    :param the_list: list of number
    :return: list of number in descending order
    Merge sorting algorithm:
        - Divide and conquer until a list has only one member
        - At the end we have a binary tree
        - Merge 2 nodes together, from bottom all the way up to head of the tree
        - Complexity of n*log(n)
            - log(n) because each step we divide by 2.
            - n because in each step, we sort linearly a list
    """

    def merge(left, right):
        """

        :param left: List of number in descending order
        :param right: List of number in descending order
        :return: Merge of left and right in descending order
        """
        union = []
        i = 0
        j = 0
        while i < len(left) and j < len(right):
            if left[i] > right[j]:
                union.append(left[i])
                i += 1
            else:
                union.append(right[j])
                j += 1
        while i < len(left):
            union.append(left[i])
            i += 1
        while j < len(right):
            union.append(right[j])
            j += 1
        return union

    descending_list = the_list[:]
    length = len(descending_list)
    if length > 1:
        middle = length // 2
        left = descending_list[0:middle]
        right = descending_list[middle:length]
        return merge(merge_sort(left), merge_sort(right))
    else:
        return descending_list