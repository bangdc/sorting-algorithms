//
// Created by bangdc on 21/10/16.
//
#include <stdio.h>
#include "sorting_algorithms.h"

int compare_array(int first_array[], int second_array[], int length) {
    if (length > 0) {
        for (int i = 0; i < length; ++i) {
            if (first_array[i] != second_array[i]) {
                return 0;
            }
        }
    }
    return 1;
}

void test_list_empty() {
    printf("Empty List Testing:\n");
    int length = 0;

    int expected_array[] = {};

    int array[] = {};
    selection_sort(array, length);
    if (compare_array(expected_array, array, length) == 1) {
        printf("\t- Selection Sort: PASSED!\n");
    } else {
        printf("\t- Selection Sort: FAILED!\n");
    }

    int array1[] = {};
    bubble_sort(array1, length);
    if (compare_array(expected_array, array1, length) == 1) {
        printf("\t- Bubble Sort: PASSED!\n");
    } else {
        printf("\t- Bubble Sort: FAILED!\n");
    }

    int array2[] = {};
    int *array3 = merge_sort(array2, length);
    if (compare_array(expected_array, array3, length) == 1) {
        printf("\t- Merge Sort: PASSED!\n");
    } else {
        printf("\t- Merge Sort: FAILED!\n");
    }
    printf("============================================================\n");
}

void test_list_one_element() {
    printf("List Of ONLY ONE Element Testing:\n");
    int length = 1;

    int expected_array[] = {1};

    int array[] = {1};
    selection_sort(array, length);
    if (compare_array(expected_array, array, length) == 1) {
        printf("\t- Selection Sort: PASSED!\n");
    } else {
        printf("\t- Selection Sort: FAILED!\n");
    }

    int array1[] = {1};
    bubble_sort(array1, length);
    if (compare_array(expected_array, array1, length) == 1) {
        printf("\t- Bubble Sort: PASSED!\n");
    } else {
        printf("\t- Bubble Sort: FAILED!\n");
    }

    int array2[] = {1};
    int *array3 = merge_sort(array2, length);
    if (compare_array(expected_array, array3, length) == 1) {
        printf("\t- Merge Sort: PASSED!\n");
    } else {
        printf("\t- Merge Sort: FAILED!\n");
    }
    printf("============================================================\n");
}

void test_list_two_elements() {
    printf("List Of Only 2 Elements Testing:\n");
    int length = 2;

    int expected_array[] = {3,2};
    
    int array[] = {2,3};
    selection_sort(array, length);
    if (compare_array(expected_array, array, length) == 1) {
        printf("\t- Selection Sort: PASSED!\n");
    } else {
        printf("\t- Selection Sort: FAILED!\n");
    }

    int array1[] = {2,3};
    bubble_sort(array1, length);
    if (compare_array(expected_array, array1, length) == 1) {
        printf("\t- Bubble Sort: PASSED!\n");
    } else {
        printf("\t- Bubble Sort: FAILED!\n");
    }

    int array2[] = {2,3};
    int *array3 = merge_sort(array2, length);
    if (compare_array(expected_array, array3, length) == 1) {
        printf("\t- Merge Sort: PASSED!\n");
    } else {
        printf("\t- Merge Sort: FAILED!\n");
    }
    printf("============================================================\n");
}

void test_list_same_elements() {
    printf("List Of Same(ONLY) Elements Testing:\n");
    int length = 3;

    int expected_array[] = {1,1,1};
    
    int array[] = {1,1,1};
    selection_sort(array, length);
    if (compare_array(expected_array, array, length) == 1) {
        printf("\t- Selection Sort: PASSED!\n");
    } else {
        printf("\t- Selection Sort: FAILED!\n");
    }

    int array1[] = {1,1,1};
    bubble_sort(array1, length);
    if (compare_array(expected_array, array1, length) == 1) {
        printf("\t- Bubble Sort: PASSED!\n");
    } else {
        printf("\t- Bubble Sort: FAILED!\n");
    }

    int array2[] = {1,1,1};
    int *array3 = merge_sort(array2, length);
    if (compare_array(expected_array, array3, length) == 1) {
        printf("\t- Merge Sort: PASSED!\n");
    } else {
        printf("\t- Merge Sort: FAILED!\n");
    }
    printf("============================================================\n");
}

void test_list_same_elements_2() {
    printf("List Of Same Elements Testing:\n");
    int length = 3;

    int expected_array[] = {2,1,1};
    
    int array[] = {1,2,1};
    selection_sort(array, length);
    if (compare_array(expected_array, array, length) == 1) {
        printf("\t- Selection Sort: PASSED!\n");
    } else {
        printf("\t- Selection Sort: FAILED!\n");
    }

    int array1[] = {1,2,1};
    bubble_sort(array1, length);
    if (compare_array(expected_array, array1, length) == 1) {
        printf("\t- Bubble Sort: PASSED!\n");
    } else {
        printf("\t- Bubble Sort: FAILED!\n");
    }

    int array2[] = {1,2,1};
    int *array3 = merge_sort(array2, length);
    if (compare_array(expected_array, array3, length) == 1) {
        printf("\t- Merge Sort: PASSED!\n");
    } else {
        printf("\t- Merge Sort: FAILED!\n");
    }
    printf("============================================================\n");
}

void test_list_increasing() {
    printf("Increased List Testing:\n");
    int length = 5;

    int expected_array[] = {5,4,3,2,1};
    
    int array[] = {1,2,3,4,5};
    selection_sort(array, length);
    if (compare_array(expected_array, array, length) == 1) {
        printf("\t- Selection Sort: PASSED!\n");
    } else {
        printf("\t- Selection Sort: FAILED!\n");
    }

    int array1[] = {1,2,3,4,5};
    bubble_sort(array1, length);
    if (compare_array(expected_array, array1, length) == 1) {
        printf("\t- Bubble Sort: PASSED!\n");
    } else {
        printf("\t- Bubble Sort: FAILED!\n");
    }

    int array2[] = {1,2,3,4,5};
    int *array3 = merge_sort(array2, length);
    if (compare_array(expected_array, array3, length) == 1) {
        printf("\t- Merge Sort: PASSED!\n");
    } else {
        printf("\t- Merge Sort: FAILED!\n");
    }
    printf("============================================================\n");
}

void test_list_decreasing() {
    printf("Decreased List Testing:\n");
    int length = 5;

    int expected_array[] = {5,4,3,2,1};

    int array[] = {5,4,3,2,1};
    selection_sort(array, length);
    if (compare_array(expected_array, array, length) == 1) {
        printf("\t- Selection Sort: PASSED!\n");
    } else {
        printf("\t- Selection Sort: FAILED!\n");
    }

    int array1[] = {5,4,3,2,1};
    bubble_sort(array1, length);
    if (compare_array(expected_array, array1, length) == 1) {
        printf("\t- Bubble Sort: PASSED!\n");
    } else {
        printf("\t- Bubble Sort: FAILED!\n");
    }

    int array2[] = {5,4,3,2,1};
    int *array3 = merge_sort(array2, length);
    if (compare_array(expected_array, array3, length) == 1) {
        printf("\t- Merge Sort: PASSED!\n");
    } else {
        printf("\t- Merge Sort: FAILED!\n");
    }
    printf("============================================================\n");
}

void test_list() {
    printf("Ordinary List Testing:\n");
    int length = 5;

    int expected_array[] = {55,33,22,21,12};
    
    int array[] = {21,12,33,55,22};
    selection_sort(array, length);
    if (compare_array(expected_array, array, length) == 1) {
        printf("\t- Selection Sort: PASSED!\n");
    } else {
        printf("\t- Selection Sort: FAILED!\n");
    }

    int array1[] = {21,12,33,55,22};
    bubble_sort(array1, length);
    if (compare_array(expected_array, array1, length) == 1) {
        printf("\t- Bubble Sort: PASSED!\n");
    } else {
        printf("\t- Bubble Sort: FAILED!\n");
    }

    int array2[] = {21,12,33,55,22};
    int *array3 = merge_sort(array2, length);
    if (compare_array(expected_array, array3, length) == 1) {
        printf("\t- Merge Sort: PASSED!\n");
    } else {
        printf("\t- Merge Sort: FAILED!\n");
    }
    printf("============================================================\n");
}

void test_list_2() {
    printf("List With Negative Number Testing:\n");
    int length = 9;

    int expected_array[] = {999,602,54,54,42,17,9,-3,-11};
    
    int array[] = {42,9,17,54,602,-3,54,999,-11};
    selection_sort(array, length);
    if (compare_array(expected_array, array, length) == 1) {
        printf("\t- Selection Sort: PASSED!\n");
    } else {
        printf("\t- Selection Sort: FAILED!\n");
    }

    int array1[] = {42,9,17,54,602,-3,54,999,-11};
    bubble_sort(array1, length);
    if (compare_array(expected_array, array1, length) == 1) {
        printf("\t- Bubble Sort: PASSED!\n");
    } else {
        printf("\t- Bubble Sort: FAILED!\n");
    }

    int array2[] = {42,9,17,54,602,-3,54,999,-11};
    int *array3 = merge_sort(array2, length);
    if (compare_array(expected_array, array3, length) == 1) {
        printf("\t- Merge Sort: PASSED!\n");
    } else {
        printf("\t- Merge Sort: FAILED!\n");
    }
    printf("============================================================\n");
}

int main() {
    test_list_empty();
    test_list_one_element();
    test_list_two_elements();
    test_list_same_elements();
    test_list_same_elements_2();
    test_list_increasing();
    test_list_decreasing();
    test_list();
    test_list_2();
    return 0;
}
