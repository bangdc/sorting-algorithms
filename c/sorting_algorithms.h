//
// Created by bangdc on 21/10/16.
//

#ifndef C_SORTING_ALGORITHMS_H
#define C_SORTING_ALGORITHMS_H

#endif //C_SORTING_ALGORITHMS_H

void swap(int *a, int *b);

void selection_sort(int array[], int length);

void bubble_sort(int array[], int length);

int *merge_sort(int array[], int length);