//
// Created by bangdc on 21/10/16.
//
#include <stdio.h>
#include <stdlib.h>
#include "sorting_algorithms.h"

/**
 *
 */
void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

int print_array(int array[], int length) {
    for (int i = 0; i < length; ++i) {
        printf("%3d ", array[i]);
    }
    printf("\n");
}

/**
 * Selection Sort Algorithm
 * DESCENDING
 * @param array  Array of integers to be sorted
 * @param length Length of the array
 */
void selection_sort(int array[], int length) {
    for (int i = 0; i < length - 1; ++i) {
        int max = i;
        for (int j = i+1; j < length; ++j) {
            if (array[j] > array[max]) {
                max = j;
            }
        }
        swap(array + i, array + max);
    }
}

/**
 * Bubble Sort Algorithm
 * DESCENDING
 *     - COMPARE and SWAP 2 consecutive elements
 *     - Stop if no more SWAP happens
 * @param array  Array of integers to be sorted
 * @param length Length of the array
 */
void bubble_sort(int array[], int length) {
    int no_more_swap = 0;
    while(!no_more_swap) {
        no_more_swap = 1;
        for (int i = 0; i < length-1; ++i)
        {
            if (array[i] < array[i+1])
            {
                swap(array+i, array+i+1);
                no_more_swap = 0;
            }
        }
    }
}

int *merge(int left[], int left_length, int right[], int right_length) {
    int *merge_array = malloc((left_length + right_length) * sizeof(int));
    int i = 0;
    int j = 0;
    int k = 0;
    while(i < left_length && j < right_length) {
        if(left[i] > right[j]) {
            *(merge_array + k) = left[i];
            i++;
        }
        else {
            *(merge_array + k) = right[j];
            j++;
        }
        k++;
    }
    while(i < left_length) {
        *(merge_array+k) = left[i];
        i++;
        k++;
    }
    while(j < right_length) {
        *(merge_array+k) = right[j];
        j++;
        k++;
    }
    return merge_array;
}

int *merge_sort(int array[], int length) {
    if(length > 1) {
        int middle = (length - 1)/ 2;
        int left_length = middle - 0 + 1;
        int right_length = (length - 1) - (middle + 1) + 1;
        int *left_array = malloc(left_length*sizeof(int));
        int *right_array = malloc(right_length*sizeof(int));
        for (int i = 0; i < left_length; ++i)
        {
            *(left_array+i) = array[i];
        }
        for (int i = 0; i < right_length; ++i)
        {
            *(right_array+i) = array[i+left_length];
        }
        return merge(merge_sort(left_array, left_length), left_length, merge_sort(right_array, right_length), right_length);
    }
    else {
        return array;
    }
}